var mongoose = require('mongoose');
var Schema = mongoose.Schema;
// User Schema
var UserSchema = new Schema({
  user_id: { type: String },
  url: { type: String },
  session_flag: { type: Boolean,  default: false},
  date: { type: Date, default: Date.now }
});

// the schema is useless so far
// we need to create a model using it
var User = mongoose.model('User', UserSchema);

// make this available to our users in our Node applications
module.exports = User;

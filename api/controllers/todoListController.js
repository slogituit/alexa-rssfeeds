var Q = require('q');
//var Feed = require('feed-to-json');
var request = require("request");
var parser = require('xml2json');
const User = require("../models/todoListModel");
const Feed = require('feed-to-json-promise')

exports.getFeeds = function(req,res){
  var defer = Q.defer();
  var url = req.query.url;
  var userId = req.query.uid;
  var flag = req.query.flag;
  console.log("userId :",userId);
  console.log("url :",url);
  console.log("typeof(url) :",typeof(url));
  if (url === undefined || url === "undefined") {
    res.send("Parameter 'url' is absent or incorrect.");
    return;
  }else{
    if(userId){
        User.find({ user_id: userId }, function(err, result) {
        if (err) throw err;
        if(!result.length){ 
          // create a new user
          var newUser = User({
            user_id: userId,
            url: url,
            session_flag: false
          });

          // save the user
          newUser.save(function(err) {
            if (err) throw err;

            console.log('value inserted!');
          });
        }
        else{
            User.findOneAndUpdate({ user_id: userId }, { url: url, session_flag :flag }, function(err, user) {
              if (err) throw err;

              // we have the updated user returned to us
              console.log("updated successful");
            });
        }
        // object of the user
      });
      
    }
    var fileExt = url.split('.').pop();
    if(fileExt == "xml"){
      var switchVar = 2;
    }
    else{
      var switchVar = 1;
    }
      var responseString = '';
      var host = extractHostname(url)
      console.log('host :',host);
      if(host.includes('www'))
          host =host.split('.')[1];
      else      
          host =host.split('.')[0];
      //var responseString = "<speak>";

    switch(switchVar) {
        case 1:
            const feed = new Feed()
            feed.load(url).then(rss => {
                try{
                  var text = rss.title.trim()
                  console.log("outer : ",text);
                  var token = '&';
                  var newToken = ' and ';
                  newStr = text.split(token).join(newToken);
                  var token1 = '<';
                  var newToken1 = ' less than ';
                  newStr = newStr.split(token1).join(newToken1);
                  var check = checkAndCreateResponceString(newStr,responseString);
                  if( check == 1 ){ 
                    responseString = responseString;
                  }
                  for (var i = 0 ; i < 6 ; i++ ){
                    var text = rss.items[i].title.trim()
                    var token = '&';
                    var newToken = ' and ';
                    newStr = text.split(token).join(newToken);
                    var token1 = '<';
                    var newToken1 = ' less than ';
                    newStr = newStr.split(token1).join(newToken1);
                    var check = checkAndCreateResponceString(newStr,responseString);
                    if( check == 1 ){ 
                        if(i==5)
                        responseString =  responseString +"<s>"+ newStr +"</s>";
                        else
                        responseString =  responseString +"<s>"+ newStr +"</s><break time=\"400ms\"/>";
                    }
                  }
                  responseString = responseString + ""
                  console.log(responseString);
                  res.send({"value" : responseString});
                  defer.resolve(true);
                }
                catch(invalidUrl){
                  console.log("invalidUrl :",invalidUrl);
                  res.send({"value" : "<speak>News feeds from this url are currently offline please try differnt url</speak>"});
                  defer.resolve(true); 
                }
                
                }).catch(error => {
              //console.error(error)
                console.log(error);
                res.send({"value" : "<speak>News feeds from this url are currently offline please try differnt url</speak>"});
                defer.resolve(true); 
            })
              break;
        case 2:

          request({url: url,json: true}, function (error, response, body) {
            if(error){
              console.log("error :",error);
            }
            try{
              console.log("inside xml");
            var xml = body;
            var result = parser.toJson(xml);
            result = JSON.parse(result);  
            var text = result.rss.channel.title.trim()
            var token = '&';
            var newToken = ' and ';
            newStr = text.split(token).join(newToken);
            var token1 = '<';
            var newToken1 = ' less than ';
            newStr = newStr.split(token1).join(newToken1);
            var check = checkAndCreateResponceString(newStr,responseString);
            if( check == 1 ){ 
              responseString = responseString;
            }
            for (var i = 0 ; i < 6 ; i++ ){
              var text = result.rss.channel.item[i].title.trim()
              var token = '&';
              var newToken = ' and ';
              newStr = text.split(token).join(newToken);
              var token1 = '<';
              var newToken1 = ' less than ';
              newStr = newStr.split(token1).join(newToken1);
              var check = checkAndCreateResponceString(newStr,responseString);
              if( check == 1 ){ 
                  responseString = responseString +"<s>"+ newStr +"</s><break time=\"400ms\"/>";
              }
            }
            responseString = responseString
            console.log(responseString);
            res.send({"value" : responseString});
            defer.resolve(true);
           }
            catch(invalidUrl){
              res.send({"value" : "<speak>News feeds from this url are currently offline please try differnt url</speak>"});
              defer.resolve(true); 
            }
                
          });
            break;
        default:
              console.log("Input RSS feed url");
              res.send("Input RSS feed url");
              defer.resolve(true);
    } 
  }
  return defer.promise;
}

function checkAndCreateResponceString(newsString,responseString) {
  if(responseString.indexOf(newsString) >= 0){
    return 0;
  } else {
    return 1;
  }
}

exports.getUser = function(req,res){
  var defer = Q.defer();
  var userId = req.query.uid;
  if (!userId) {
    res.send("Parameter 'uid' is absent or incorrect.");
    return;
  }else{
      User.find({ user_id: userId }, function(err, result) {
          if (err) throw err;
          console.log("user_id :",userId) ;
          if(result[0]){
            res.send(result[0]);
            defer.resolve(true);
          }
          else{
            console.log("inside else :");
            res.send(false);
            defer.resolve(true);
          }
          // object of the user
      });
  }
  return defer.promise;
}


function extractHostname(url) {
    var hostname;
    //find & remove protocol (http, ftp, etc.) and get hostname

    if (url.indexOf("://") > -1) {
        hostname = url.split('/')[2];
    }
    else {
        hostname = url.split('/')[0];
    }

    //find & remove port number
    hostname = hostname.split(':')[0];
    //find & remove "?"
    hostname = hostname.split('?')[0];

    return hostname;
}

exports.deleteUser = function(req,res){
  var defer = Q.defer();
  var userId = req.query.uid;
  if (!userId) {
    res.send("Parameter 'uid' is absent or incorrect.");
    return;
  }else{
     User.findOneAndRemove({ user_id: userId }, function(err) {
      if (err) throw err;

      // we have deleted the user
      console.log('User deleted!');
      res.send("User deleted");
    });
  }
  return defer.promise;
}
